package fr.snyker.bots.entity;

import java.math.BigInteger;

public class Inventory {

    private int matiereNoire;

    private BigInteger poussieres;
    private BigInteger gaz;

    public Inventory(){
        this.matiereNoire = 1;

        this.poussieres = BigInteger.valueOf(0);
        this.gaz = BigInteger.valueOf(0);
    }

    /**
     * Get the material MatiereNoire for the
     * speed boost exploration
     * @return matiereNoire
     */
    public int getMatiereNoir() {
        return matiereNoire;
    }

    public int addMatiereNoir(int value){
        if(matiereNoire + value < 1) {
            return (this.matiereNoire = 0);
        }
        matiereNoire += value;
        return this.matiereNoire;
    }

    public BigInteger getPoussieres() {
        return poussieres;
    }

    public BigInteger getGaz() {
        return gaz;
    }

}
