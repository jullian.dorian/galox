package fr.snyker.bots.entity;

import fr.snyker.bots.Galox;
import fr.snyker.bots.resources.ResourceLocation;
import fr.snyker.bots.resources.emotes.EnumEmote;
import net.dv8tion.jda.client.managers.EmoteManager;
import net.dv8tion.jda.client.managers.EmoteManagerUpdatable;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.entities.impl.EmoteImpl;
import net.dv8tion.jda.core.requests.restaction.AuditableRestAction;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class EntityPlayer {

    /* -- DISCORD -- */
    /* User */
    private transient User user;

    /* -- Galox -- */
    //Tutoriel , si le joueur la fait ou non
    public boolean tutoriel;

    //Level of player
    private int level;
    //Experience of player
    private long experience;
    //Total Experience winned
    private long totalExperience;
    //Money of the player
    private long klox;
    //The inventory of Player
    private Inventory inventory;

    private transient ResourceLocation resourceLocation;

    public EntityPlayer(){}

    /**
     * Create a player of the game
     * @param user
     */
    public void create(User user){
        this.user = user;

        this.level = 1;
        this.experience = 0;
        this.totalExperience = 0;
        this.klox = 0;
        this.inventory = new Inventory();

        //creation du fichier
        resourceLocation = new ResourceLocation(ResourceLocation.Locations.USERS, String.valueOf(user.getIdLong()) + ".json");
        resourceLocation.saveFile(this);
    }

    /**
     * Delete a entityPlayer and this file
     */
    public boolean delete(){
        this.resourceLocation.deleteFile();
        return Galox.MANAGER.getPlayerManager().removePlayer(this);
    }

    /**
     * Sauvegarde le contenue du joueur dans le fichier
     * @see ResourceLocation#saveFile(EntityPlayer)
     */
    public void save(){
        getResourceLocation().saveFile(this);
    }

    /*
    --- GETTER AND SETTER ---
     */

    /**
     * Get the User Client Discord of a EntityPlayer
     * @return User - The User Client
     */
    public User getUser() {
        return user;
    }

    /**
     * Define a new User Client of the EntityPlayer
     * @param user - New User
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the level of the EntityPlayer
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set the level of the EntityPlayer
     * @param level - New level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Add a level of the entityPlayer,
     * you can be added (+) or remove (-)
     * @param value - The value added
     * @return 0 - If the level under 1
     */
    public int addLevel(int value){
        if(this.level + value > 0) return this.level += value;
        return 0;
    }

    /**
     * Get the actuel experience of entityPlayer
     * @return experience
     */
    public long getExperience() {
        return experience;
    }

    /**
     * Set new experience of the entityPlayer
     * @param experience
     */
    public void setExperience(long experience) {
        this.experience = experience;
    }

    public void addExperience(long value) {

        addTotalExperience(value);

        if(this.experience + value < getCourbe()){
            this.experience += value;
        } else if(this.experience + value == getCourbe()){
            this.experience = 0;
            addLevel(1);
        } else {

            long current = experience + value;

            do {
                if(current < getCourbe()) break;
                current = current - (long) getCourbe();
                addLevel(1);
            } while (current > getCourbe());

            setExperience(current);
        }


    }

    protected double getCourbe(){
        return getCourbe(this.level);
    }

    public double getCourbe(int level){
        return 25 * Math.pow(level + 1, 2);
    }

    /**
     * Get a total experience earned of the entityPlayer
     * @return totalExperience
     */
    public long getTotalExperience() {
        return totalExperience;
    }

    /**
     * Add a totalExperience of the entityPlayer,
     * you can be added (+) or remove (-)
     * @param value - The value added
     * @return 0 - If the level under 1
     */
    private long addTotalExperience(long value){
        if(this.totalExperience + value > 0) return this.totalExperience += value;
        return 0;
    }

    /**
     * Get the money Klox of the EntityPlayer
     * @return Klox - Money
     */
    public long getKlox() {
        return klox;
    }

    /**
     * Set a new
     * @param klox
     */
    public void setKlox(long klox) {
        this.klox = klox;
    }

    /**
     * Add the money klox on the EntityPlayer
     * You can add (+) or remove (-)
     * @param value - The value added
     * @return klox
     */
    public long addKlox(long value) {
        if(this.klox + value > 0) return this.klox += value;
        return 0;
    }

    /**
     * Add a random value on the money of player
     * random effective on 1 at level * 2
     * @see #addRandomKlox(long, long) - The used fonction
     * @return random
     */
    public long addRandomKlox(){
        return addRandomKlox(1L, (long) (this.level * 3));
    }

    /**
     * Add a random value on the money of the player
     * @param min - The min value
     * @param max - The max value
     * @return random
     */
    public long addRandomKlox(long min, long max){
        long random = ThreadLocalRandom.current().nextLong(min, max);
        addKlox(random);
        return random;
    }

    /**
     * Get the inventory of player
     * @return Inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get the resourceLocation of the player
     * @see ResourceLocation
     * @return ResourceLocation
     */
    public ResourceLocation getResourceLocation() {
        return resourceLocation;
    }

    public void setResourceLocation(ResourceLocation resourceLocation) {
        this.resourceLocation = resourceLocation;
    }

    /**
     * Get the embed build of the player
     * @return EmbedBuilder
     */
    public EmbedBuilder getEmbedPlayer(){

        final String wallet = "Porte-Monnaie";
        final String backpack = "Sac à dos";

        return new EmbedBuilder()
                .setAuthor("Sac à dos, " + user.getName(), null, user.getAvatarUrl())
                .setDescription("Voici les informations que vous possédez sur votre Univers.")
                .setColor(new Color(0x3C5937))
                //Informations leveling
                .addField("Niveau", String.valueOf(this.level), true)
                .addField("Experience", String.valueOf((float) (this.experience / 100)) + " / 100%", true)
                .addField("Total Experience", String.valueOf(this.totalExperience), true)

                .addField(EnumEmote.MONEY_BAG.getMention() + " " + wallet,EnumEmote.KLOX.getMention() + " Klox : " + this.klox, true)
                //Sac à dos
                .addField(backpack, "Matière Noire : " + getInventory().getMatiereNoir() +
                            "\n" +
                            "Gaz : " + getInventory().getGaz() +
                            "\n" +
                            "Poussières : " + getInventory().getPoussieres(), false)
                ;
    }

}
