package fr.snyker.bots.game.tutoriel;

import fr.snyker.bots.Galox;
import fr.snyker.bots.entity.EntityPlayer;
import net.dv8tion.jda.core.entities.PrivateChannel;

public class Tutoriel {

    private PrivateChannel privateChannel;
    private EntityPlayer entityPlayer;

    public Tutoriel(EntityPlayer entityPlayer){
        this.entityPlayer = entityPlayer;
        this.entityPlayer.tutoriel = true;

        this.privateChannel = entityPlayer.getUser().openPrivateChannel().complete();

        expGame();
        expUnivers();
        start();

        this.entityPlayer.save();
    }

    private void expGame(){
        this.privateChannel.sendMessage("===============[TUTORIEL - Explication du jeu]===============").queue();

        this.privateChannel.sendMessage("Bonjour ! Je suis Galox, Maître du temps et de l'espace. " +
                "Avant de commencer à t'amuser avec ta galaxie, il est nécessaire de savoir comment fonctionne le jeu.").queue();

        this.privateChannel.sendMessage("Le but du jeu étant de créer son univers, de recolter des ressources, de créer ses planètes et s'amuser à" +
                " être Dieu. Plus précisement, quand vous débuter le jeu, vous devez en premier temps récolter des ressources pour créer votre " +
                "première planète. C'est vous qui choissisez quelles ressources vous voulez que votre planète génère. Vous trouverez le menu des " +
                "planètes avec la commande suivante : !game planete help").queue();
    }

    private void expUnivers(){
        this.privateChannel.sendMessage("===============[TUTORIEL - L'univers]===============").queue();

        this.privateChannel.sendMessage("Actuellement tu possède 1 matière noire, celle-ci est une matière extremement rare, " +
                "je t'en donne 1 toutes les 24 heures. Elle te permet de faire des explorations éclaires et de ramener des " +
                "quantités énormes de ressources, de l'argent et de l'experience.").queue();

        this.privateChannel.sendMessage("Tu ne peux avoir que un seul univers, mais celui-ci peux contenir énormement de système solaire et de " +
                "planète. Tout dépend dans quelle univers tu veux vivre.").queue();

        this.privateChannel.sendMessage("Des trous noires peuvent également se créer n'importe quand, n'importe ou, mais cela à plus de chance " +
                "d'arriver lorsque tu détruit une planète ou un système solaire.").queue();
    }

    private void start(){
        this.privateChannel.sendMessage("===============[TUTORIEL - Commencer le jeu]===============").queue();

        this.privateChannel.sendMessage("Bien, il me semble t'avoir tout dit, si tu veux en savoir plus la commande d'aide : < !game help > sera " +
                "tout le temps disponible pour te guider.").queue();

        this.privateChannel.sendMessage("Il est temps de commencer le jeu. Rend-toi sur un salon de jeu, et tu n'a plus cas faire la commande " +
                "suivante : < !game exploreall >").queue();
    }

}
