package fr.snyker.bots.commands.utils;

import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public abstract class SubCommand {

    protected User user;
    protected String[] args;
    protected MessageReceivedEvent event;
    protected TextChannel textChannel;

    public SubCommand(User user, String[] args, MessageReceivedEvent event, TextChannel textChannel){
        this.user = user;
        String[] a = new String[args.length - 1];
        for(int i = 0; i < a.length; i++){
            a[i] = args[i+1];
        }
        this.args = a;
        this.event = event;
        this.textChannel = textChannel;
    }

    public abstract boolean invoke();

}
