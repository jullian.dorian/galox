package fr.snyker.bots.commands.utils;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public interface ICommand {

    String getPrefix();

    String getDescription();

    String[] getAliases();

    boolean invoke(User user, String[] args, MessageReceivedEvent event, TextChannel textChannel);

}
