package fr.snyker.bots.commands.utils;

import fr.snyker.bots.Galox;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public abstract class Command implements ICommand {

    private static Command current = null;

    public static Command getCommand(Event event){

        if(event instanceof MessageReceivedEvent){
            final MessageReceivedEvent messageReceivedEvent = (MessageReceivedEvent) event;

            String message = messageReceivedEvent.getMessage().getContentDisplay();
            String[] args = message.split(" ");

            String tag = getTagFromMessage(args[0]);
            if(tag.equals("")) return null;

            String prefix = getPrefixFromMessage(args[0]);
            if(prefix.equals("")) return null;

            for(Command command : Galox.MANAGER.getCommandManager().getCommandsRegistries()){
                current = command;

                if(command.getPrefix().equalsIgnoreCase(prefix)){
                    return command;
                }

                for(String aliase : command.getAliases()){
                    if(aliase.equalsIgnoreCase(prefix)){
                        return command;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Send the command
     * @param messageReceivedEvent - Event
     * @return True - if the command has been sended
     */
    public boolean send(MessageReceivedEvent messageReceivedEvent){
        String message = messageReceivedEvent.getMessage().getContentDisplay();

        User user = messageReceivedEvent.getAuthor();
        String[] args = getArgsNoPrefix(message);

        return current.invoke(user, args, messageReceivedEvent, messageReceivedEvent.getTextChannel());
    }

    private String[] getArgsNoPrefix(String message){
        String[] olds = message.split(" ");
        String[] args = new String[olds.length-1];

        for(int i = 1; i < olds.length; i++){
            args[i-1] = olds[i];
        }
        return args;
    }

    /**
     * Get the prefix of a command
     * @param message - The message
     * @return Prefix
     */
    private static String getPrefixFromMessage(String message){
        return message.substring(1, message.length());
    }

    /**
     * Get the tag from the message
     * @param message - The message
     * @return Tag
     */
    private static String getTagFromMessage(String message){
        if(message.length() > 1){
            String tag = message.substring(0, 1);

            for(CommandTags commandTags : CommandTags.values()){
                if(commandTags.getCommandTag().equals(tag)){
                    return tag;
                }
            }
        }
        return "";
    }

    protected String getTextAliases(String[] aliases){
        StringBuilder stringBuilder = new StringBuilder();

        for(String s : aliases){
            stringBuilder.append(s + ", ");
        }
        return stringBuilder.delete(stringBuilder.length()-2, stringBuilder.length()).toString();
    }

    /**
     * Class of tags of the command
     */
    protected enum CommandTags{
        CMD1('!'),
        CMD2('?'),
        CMD3('/'),
        CMD4('>'),
        CMD5('<'),
        CMD6('.');

        private char commandTag;

        CommandTags(char commandTag){
            this.commandTag = commandTag;
        }

        public char getTag(){
            return commandTag;
        }

        public String getCommandTag() {
            return String.valueOf(getTag());
        }
    }
}
