package fr.snyker.bots.commands.game;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.Command;
import fr.snyker.bots.entity.EntityPlayer;
import fr.snyker.bots.game.tutoriel.Tutoriel;
import fr.snyker.bots.managers.entities.PlayerManager;
import fr.snyker.bots.resources.ResourceImages;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import javax.xml.soap.Text;
import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class CommandGame extends Command {

    @Override
    public String getPrefix() {
        return "game";
    }

    @Override
    public String getDescription() {
        return "Utilisez pour utiliser les commandes du jeu";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"jeu"};
    }

    @Override
    public boolean invoke(User user, String[] args, MessageReceivedEvent event, TextChannel channel) {

        final PlayerManager playerManager = Galox.MANAGER.getPlayerManager();

        if(args.length >= 1){

            switch (args[0]){
                case "create":
                    if(playerManager.contains(user)){
                        channel.sendMessage("Vous possédez déjà un compte de jeu !").queue();
                    } else {

                        EntityPlayer entityPlayer = new EntityPlayer();
                        entityPlayer.create(user);

                        playerManager.addPlayer(entityPlayer);

                        //Confirmation de la création avec le message et une image random
                        ResourceImages randomImage = ResourceImages.getRandomResource(ResourceImages.Type.IMG);
                        if(!randomImage.isUrl()){
                            File file = randomImage.getResourceLocation().getFile();

                            MessageBuilder messageBuilder = new MessageBuilder();
                            messageBuilder.append(user.getAsMention() +" : Création de votre galaxie avec " +
                                            "succès. N'oublie pas de regardez en privée pour le tutoriel.");


                            if(file.getName().contains("5")){
                                entityPlayer.getInventory().addMatiereNoir(1);
                                messageBuilder.append("\n");
                                messageBuilder.append("Grâce à votre chance, vous gagnez 1 matière noire en plus !");
                            }

                            channel.sendFile(randomImage.getResourceLocation().getFile(), messageBuilder.build()).queue();

                        }

                        //Envoie du tutoriel en privée
                        if(!entityPlayer.tutoriel){
                            new Tutoriel(entityPlayer);
                        }

                    }

                    break;

                case "delete":
                    if(!hasAccount(channel, user)) return false;

                    new SubCommandDelete(user, args, event, channel).invoke();
                    break;

                case "explore":
                    if(!hasAccount(channel, user)) return false;

                    break;

                case "exploreall":
                    if(!hasAccount(channel, user)) return false;

                    new SubCommandExploreAll(user, args, event, channel).invoke();
                    break;

                case "planete":
                    if(!hasAccount(channel, user)) return false;

                    new SubCommandPlanete(user, args, event, channel).invoke();
                    break;

                case "help":
                    EmbedBuilder messageEmbed = new EmbedBuilder()
                            .setAuthor(Galox.JDA.getSelfUser().getName(), null, Galox.JDA.getSelfUser().getAvatarUrl())
                            .setTitle("Voici la liste des arguments pour la commande : " + getPrefix())
                            .setColor(new Color(0x4C4E88))
                            .setDescription("Pour effectuer une commande à plusieurs arguments essayez : <command> <arg0> <arg1> etc...");

                    messageEmbed.addField("create", "Créer un compte pour le début du jeu", false);
                    messageEmbed.addField("delete", "Supprime votre compte de jeu", false);
                    messageEmbed.addField("explore", "Vous faites une exploration dans la Galaxie", false);
                    messageEmbed.addField("exploreall", "Vous faites une exploration éclaire dans la Galaxie, pour les flemmards.", false);

                    channel.sendMessage(messageEmbed.build()).queue();
                    break;
            }

        }

        return false;
    }

    private boolean hasAccount(TextChannel textChannel, User user){
        if(Galox.MANAGER.getPlayerManager().contains(user)){
            return true;
        }
        textChannel.sendMessage("Vous devez possédez un compte pour effectuer cette action.\n" +
                "Pour créer une compte saisissez la commande suivante : !game create").queue();
        return false;
    }
}
