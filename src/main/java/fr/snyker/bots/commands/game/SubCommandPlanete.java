package fr.snyker.bots.commands.game;

import fr.snyker.bots.commands.utils.SubCommand;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SubCommandPlanete extends SubCommand {

    public SubCommandPlanete(User user, String[] args, MessageReceivedEvent event, TextChannel textChannel) {
        super(user, args, event, textChannel);
    }

    @Override
    public boolean invoke() {

        if(args.length == 0){
            textChannel.sendMessage("Help planete").queue();
        }

        return false;
    }
}
