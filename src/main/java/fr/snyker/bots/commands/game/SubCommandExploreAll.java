package fr.snyker.bots.commands.game;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.SubCommand;
import fr.snyker.bots.entity.EntityPlayer;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.concurrent.ThreadLocalRandom;

public class SubCommandExploreAll extends SubCommand{

    public SubCommandExploreAll(User user, String[] args, MessageReceivedEvent event, TextChannel channel) {
        super(user, args, event, channel);
    }

    @Override
    public boolean invoke() {

        ThreadLocalRandom localRandom = ThreadLocalRandom.current();

        final EntityPlayer entityPlayer = Galox.MANAGER.getPlayerManager().getPlayer(user);
        entityPlayer.getInventory().addMatiereNoir(-1);
        int level = entityPlayer.getLevel();

        //Attribution des valeurs
        int valueMatBlack = 10 ^ (level / 10);

        int min = valueMatBlack * (level / 100);
        int max = valueMatBlack * (level / 10);

        //loots
        int klox = localRandom.nextInt(2, 50);
        long experience = localRandom.nextInt(klox / 2, klox);
        int perMatBlack = localRandom.nextInt(0, 10000);
        int qttMatBlack = perMatBlack > 8000 ? localRandom.nextInt(0, 2) : 0;

        //Attribution des loots
        entityPlayer.addKlox(klox);
        entityPlayer.addExperience(experience);
        entityPlayer.getInventory().addMatiereNoir(qttMatBlack);

        textChannel.sendMessage(user.getAsMention() + " Lors de votre virée dans l'espace-temps vous avez recolté un total de : ")
                .append("Experience : ").append(String.valueOf(experience))
                .append("Matière Noire (x").append(String.valueOf(qttMatBlack)).append("), ")
                .append("Klox : (x").append(String.valueOf(klox)).append(")")
                .queue();

        return false;
    }
}
