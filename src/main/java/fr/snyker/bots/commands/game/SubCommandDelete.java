package fr.snyker.bots.commands.game;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.SubCommand;
import fr.snyker.bots.entity.EntityPlayer;
import fr.snyker.bots.resources.ResourceImages;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SubCommandDelete extends SubCommand {

    public SubCommandDelete(User user, String[] args, MessageReceivedEvent event, TextChannel textChannel) {
        super(user, args, event, textChannel);
    }

    @Override
    public boolean invoke() {
        if(args.length >= 1){
            if(args[0].equalsIgnoreCase("confirm")){
                EntityPlayer entityPlayer = Galox.MANAGER.getPlayerManager().getPlayer(user);
                if(entityPlayer.delete()){
                    ResourceImages resourceImages = ResourceImages.getRandomResource(ResourceImages.Type.GIF);
                    if(!resourceImages.isUrl()) {
                        textChannel.sendFile(resourceImages.getResourceLocation().getFile(),
                                new MessageBuilder(user.getAsMention() + " Votre univers viens d'être détruit.")
                                        .build()).queue();
                    }
                } else {
                    textChannel.sendMessage("Oups ! Une erreur est survenu, veuillez réessayez.").queue();
                }
            }
        } else {
            textChannel.sendMessage("Vous voulez vraiment supprimé votre univers ? Cette action est irreversible !\n" +
                    "Pour confimer saisissez la commande suivante : !game delete confirm").queue();
        }
        return false;
    }
}
