package fr.snyker.bots.commands;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.Command;
import fr.snyker.bots.entity.EntityPlayer;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class CommandCard extends Command {
    @Override
    public String getPrefix() {
        return "card";
    }

    @Override
    public String getDescription() {
        return "Affiche votre carte d'information, cela contient votre inventaire, vos niveaux etc...";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"carte", "infos"};
    }

    @Override
    public boolean invoke(User user, String[] args, MessageReceivedEvent event, TextChannel textChannel) {

        if(user.isBot()) return false;

        EntityPlayer entityPlayer = Galox.MANAGER.getPlayerManager().getPlayer(user);
        if(entityPlayer != null){

            textChannel.sendMessage(entityPlayer.getEmbedPlayer().build()).queue();

        }

        return false;
    }
}
