package fr.snyker.bots.commands;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.Command;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.awt.*;

public class CommandHelp extends Command {

    @Override
    public String getPrefix() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Affiche la liste des commandes";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"h", "aide"};
    }

    @Override
    public boolean invoke(User user, String[] args, MessageReceivedEvent event, TextChannel channel) {

        StringBuilder tags = new StringBuilder();
        for(CommandTags commandTags : Command.CommandTags.values()) tags.append(commandTags.getCommandTag()+", ");
        tags.delete(tags.length()-2,tags.length());

        //Creation de l'embed message
        EmbedBuilder messageEmbed = new EmbedBuilder()
                .setAuthor(Galox.JDA.getSelfUser().getName(), null, Galox.JDA.getSelfUser().getAvatarUrl())
                .setTitle("Voici la liste des commandes")
                .setColor(new Color(0x4A8831))
                .setDescription("Pour effectuer une commande vous pouvez utiliser l'un des tags suivant : " + tags.toString());

        //Ajout des fields avec les commandes
        for(Command command : Galox.MANAGER.getCommandManager().getCommandsRegistries()){
            messageEmbed.addField(command.getPrefix(), command.getDescription() + "\n" + "Aliases : " + getTextAliases(command.getAliases()), false);
        }

        channel.sendMessage(messageEmbed.build()).queue();

        return false;
    }
}
