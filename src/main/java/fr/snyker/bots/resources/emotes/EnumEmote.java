package fr.snyker.bots.resources.emotes;

import fr.snyker.bots.Galox;
import net.dv8tion.jda.core.JDA;

public enum EnumEmote {

    /* EMOTE DISCORD */
    MONEY_BAG("<:moneybag:405312883950616576>"),

    /* GALOX UTILS */
    KLOX (Galox.getMainGuild().getEmotesByName("klox", true).get(0).getAsMention()),
    STARS("<:star:404338858038132756>");

    private String mention;

    EnumEmote(String mention){
        this.mention = mention;
    }

    public String getMention() {
        return mention;
    }
}
