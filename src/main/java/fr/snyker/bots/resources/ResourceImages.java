package fr.snyker.bots.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public enum ResourceImages {

    UNIVERSE_CREATED_1_JPG(Type.IMG, new ResourceLocation(ResourceLocation.Locations.IMAGES, "galaxie1.jpg")),
    UNIVERSE_CREATED_1_PNG(Type.IMG, new ResourceLocation(ResourceLocation.Locations.IMAGES, "galaxie2.png")),
    UNIVERSE_CREATED_2_PNG(Type.IMG, new ResourceLocation(ResourceLocation.Locations.IMAGES, "galaxie3.png")),
    UNIVERSE_CREATED_3_PNG(Type.IMG, new ResourceLocation(ResourceLocation.Locations.IMAGES, "galaxie4.png")),
    UNIVERSE_CREATED_2_JPG(Type.IMG, new ResourceLocation(ResourceLocation.Locations.IMAGES, "galaxie5.jpg")),

    UNIVERSE_DISBAND_1_GIF(Type.GIF, new ResourceLocation(ResourceLocation.Locations.IMAGES, "universe_explose.gif"));

    private Type type;
    private String url;

    private ResourceLocation resourceLocation;

    ResourceImages(Type type, String url){
        this.type = type;
        this.url = url;
        this.resourceLocation = null;
    }

    ResourceImages(Type type, ResourceLocation resourceLocation){
        this.type = type;
        this.url = null;
        this.resourceLocation = resourceLocation;
    }

    public Type getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public ResourceLocation getResourceLocation() {
        return resourceLocation;
    }

    /**
     * Check if the ResourceImages is an Image with Url or a File
     * @return True if is an Image with Url
     */
    public boolean isUrl(){
        return url != null;
    }

    /**
     * Get a random resource with all image
     * @return Image
     */
    public static ResourceImages getRandomResource(){
        int r = ThreadLocalRandom.current().nextInt(0, ResourceImages.values().length);
        return ResourceImages.values()[r];
    }

    /**
     * Get a random resource image with a Type
     * @param type - The type of Image
     * @return Random Image
     */
    public static ResourceImages getRandomResource(Type type){
        List<ResourceImages> resourceImagesType = new ArrayList<>();
        for(int i = 0; i < values().length; i++){
            ResourceImages currentResource = values()[i];
            if(currentResource.getType() == type){
                resourceImagesType.add(currentResource);
            }
        }
        int r = ThreadLocalRandom.current().nextInt(0, resourceImagesType.size());
        return resourceImagesType.get(r);
    }

    public enum Type{
        IMG,
        GIF
    }
}
