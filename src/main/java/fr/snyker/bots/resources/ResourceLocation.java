package fr.snyker.bots.resources;

import fr.snyker.bots.entity.EntityPlayer;
import fr.snyker.bots.managers.entities.PlayerManager;

import java.io.*;

public class ResourceLocation {

    public static final String PATH_RESOURCES = "src/main/resources";

    private File file;

    public ResourceLocation(Locations locations, String fileName){
        this(locations.getPath() + "/" + fileName);
    }

    private ResourceLocation(String pathFile){
        file = new File(PATH_RESOURCES, pathFile);
    }

    public void saveFile(EntityPlayer entityPlayer){
        try {
            String content = PlayerManager.serialize(entityPlayer);

            file.getParentFile().mkdirs();
            file.createNewFile();

            final FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteFile(){
        System.out.println(file.getName());
        return file.delete();
    }

    public static String loadContent(File file){
        try{

            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            final StringBuilder stringBuilder = new StringBuilder();

            String line;

            while((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line);
            }

            bufferedReader.close();
            return stringBuilder.toString();

        } catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }

    public File getFile() {
        return file;
    }

    public static enum Locations{

        IMAGES("images"),
        USERS("users");

        private String path;

        Locations(String path){
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }

}
