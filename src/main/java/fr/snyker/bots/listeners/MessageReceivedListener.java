package fr.snyker.bots.listeners;

import fr.snyker.bots.Galox;
import fr.snyker.bots.commands.utils.Command;
import fr.snyker.bots.entity.EntityPlayer;
import fr.snyker.bots.resources.ResourceLocation;
import net.dv8tion.jda.client.managers.EmoteManager;
import net.dv8tion.jda.client.managers.EmoteManagerUpdatable;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.impl.EmoteImpl;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.requests.restaction.AuditableRestAction;

import java.io.File;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MessageReceivedListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event){

        if(event.getAuthor().isBot()) return;
        if(event.getPrivateChannel() != null) return;

        Command command = Command.getCommand(event);
        if(command != null){
            command.send(event);
            return;
        }

        EntityPlayer entityPlayer = Galox.MANAGER.getPlayerManager().getPlayer(event.getAuthor());
        if(entityPlayer != null) entityPlayer.addRandomKlox();


    }

}
