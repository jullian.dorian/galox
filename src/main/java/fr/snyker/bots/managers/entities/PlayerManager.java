package fr.snyker.bots.managers.entities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.snyker.bots.entity.EntityPlayer;
import net.dv8tion.jda.core.entities.User;

import java.util.HashSet;
import java.util.Set;

public class PlayerManager {

    private static Gson gson;

    private Set<EntityPlayer> entityPlayers = new HashSet<>();

    public PlayerManager(){
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .enableComplexMapKeySerialization()
                .create();
    }

    /**
     * Add a player on the SetMap
     * @param entityPlayer - The EntityPlayer added
     */
    public void addPlayer(EntityPlayer entityPlayer){
        entityPlayers.add(entityPlayer);
    }

    /**
     * Get the player on the Setmap
     * @param user - The user discord
     * @return EntityPlayer if exist on the SetMap
     */
    public EntityPlayer getPlayer(User user){
        if(contains(user)){
            for(EntityPlayer entityPlayer : entityPlayers){
                if(entityPlayer.getUser() == user){
                    return entityPlayer;
                }
            }
        }
        return null;
    }

    public boolean removePlayer(EntityPlayer entityPlayer){
        return this.entityPlayers.remove(entityPlayer);
    }

    /**
     * Chech if the entityPlayers list contains a user
     * @param user - The user checked
     * @return True if the user exist
     */
    public boolean contains(User user){
        for(EntityPlayer entityPlayer : entityPlayers){
            if(entityPlayer.getUser() == user){
                return true;
            }
        }
        return false;
    }

    public static String serialize(EntityPlayer entityPlayer){
        return gson.toJson(entityPlayer);
    }

    public static EntityPlayer deserialize(String text){
        return gson.fromJson(text, EntityPlayer.class);
    }

    public Set<EntityPlayer> getEntityPlayers() {
        return entityPlayers;
    }
}
