package fr.snyker.bots.managers.commands;

import fr.snyker.bots.commands.utils.Command;

import java.util.HashSet;
import java.util.Set;

public class CommandManager {

    private Set<Command> commandsRegistries = new HashSet<>();

    public CommandManager(){}

    /**
     * Add a new command of Set<Command>
     * @param command - The command added
     */
    public void registerCommand(Command command){
        commandsRegistries.add(command);
    }

    /**
     * Get the commands registries of the bot
     * @return - All Commands
     */
    public Set<Command> getCommandsRegistries() {
        return commandsRegistries;
    }

}
