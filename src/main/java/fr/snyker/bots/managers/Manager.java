package fr.snyker.bots.managers;

import fr.snyker.bots.commands.CommandCard;
import fr.snyker.bots.commands.game.CommandGame;
import fr.snyker.bots.commands.CommandHelp;
import fr.snyker.bots.listeners.MessageReceivedListener;
import fr.snyker.bots.managers.commands.CommandManager;
import fr.snyker.bots.managers.entities.PlayerManager;
import fr.snyker.bots.managers.listeners.ListenersManager;
import net.dv8tion.jda.core.JDA;

public class Manager {

    private JDA jda;

    private CommandManager commandManager;
    private ListenersManager listenersManager;
    private PlayerManager playerManager;

    public Manager(JDA jda){
        this.jda = jda;
    }

    public void init(){
        this.commandManager = new CommandManager();
        this.listenersManager = new ListenersManager(jda);
        this.playerManager = new PlayerManager();
    }

    public void register(){
        commandManager.registerCommand(new CommandHelp());
        commandManager.registerCommand(new CommandGame());
        commandManager.registerCommand(new CommandCard());

        listenersManager.registerEvent(new MessageReceivedListener());
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public ListenersManager getListenersManager() {
        return listenersManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }
}
