package fr.snyker.bots.managers.listeners;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ListenersManager {

    private JDA jda;

    public ListenersManager(JDA jda){
        this.jda = jda;
    }

    public void registerEvent(ListenerAdapter listenerAdapter){
        jda.addEventListener(listenerAdapter);
    }

}
