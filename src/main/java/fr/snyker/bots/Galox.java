package fr.snyker.bots;

import fr.snyker.bots.entity.EntityPlayer;
import fr.snyker.bots.managers.Manager;
import fr.snyker.bots.managers.entities.PlayerManager;
import fr.snyker.bots.resources.ResourceLocation;
import javafx.concurrent.Task;
import jdk.nashorn.internal.scripts.JD;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.requests.Route;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Galox {

    public static JDA JDA;

    public static Manager MANAGER;
    public static final Logger LOGGER = Logger.getLogger("JDA");

    public Galox() throws LoginException, InterruptedException {
        JDA = new JDABuilder(AccountType.BOT)
                .setToken("NFnEUWTBw")
                .setGame(Game.of(Game.GameType.DEFAULT, "l'univers"))
                .buildBlocking();

        MANAGER = new Manager(JDA);

    }

    /**
     * Return the Main Guild of the Bot has the stocked emojis
     * @return Guild
     */
    public static Guild getMainGuild(){
        return JDA.getGuildById(288404867377135617L);
    }

    /**
     * Initialize all variables and register them
     */
    public void initAndRegister(){
        MANAGER.init();

        MANAGER.register();

        reloadPlayers();

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> {

            for(EntityPlayer entityPlayer : MANAGER.getPlayerManager().getEntityPlayers()){
                entityPlayer.save();
            }
            LOGGER.info("Sauvegarde des joueurs !");
        };
        executor.scheduleWithFixedDelay(task, 0, 120, TimeUnit.SECONDS);
    }

    /**
     * Reload all the players and add on the list
     *
     * This reload is called when the bot start
     */
    private void reloadPlayers(){
        File path = new File(ResourceLocation.PATH_RESOURCES + "/" + ResourceLocation.Locations.USERS.getPath());
        path.mkdirs();
        for(File file : path.listFiles()){
            long idUser = Long.valueOf(file.getName().substring(0, file.getName().length() - 5));
            User user = JDA.getUserById(idUser);
            EntityPlayer entityPlayer = PlayerManager.deserialize(ResourceLocation.loadContent(file));
            entityPlayer.setUser(user);
            entityPlayer.setResourceLocation(new ResourceLocation(ResourceLocation.Locations.USERS, idUser + ".json"));

            MANAGER.getPlayerManager().addPlayer(entityPlayer);
        }
    }

}
