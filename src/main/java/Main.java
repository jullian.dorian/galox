import fr.snyker.bots.Galox;

import javax.security.auth.login.LoginException;

public class Main {

    public static void main(String[] args) throws LoginException, InterruptedException {
        Galox galox = new Galox();

        galox.initAndRegister();
    }

}
